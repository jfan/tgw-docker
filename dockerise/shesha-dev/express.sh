#! /bin/bash

docker build -t shesha-dev $PWD
docker run \
    --security-opt apparmor:unconfined \
    --cap-add=SYS_ADMIN \
    --gpus all \
    --name shesha-dev \
    --volume "$PWD/shesha:/home/docker/shesha" \
    --rm \
    -p "18888:8888" \
    "$@" \
    -it shesha-dev
