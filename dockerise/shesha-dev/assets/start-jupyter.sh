#! /bin/bash

nohup jupyter lab --ip=0.0.0.0 --allow-root &> $HOME/.log_jupyter &
