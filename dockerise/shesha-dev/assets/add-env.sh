export CONDA_ROOT=/home/docker/miniconda3
export PATH=$CONDA_ROOT/bin:$PATH
export SHESHA_ROOT=/home/docker/shesha
export PYTHONPATH=$SHESHA_ROOT:$PYTHONPATH
export PYTHONDONTWRITEBYTECODE=1
export ADD_ENV=1

echo "Added environment variables from docker container"
