#! /bin/bash

if [ ! -d "$SHESHA_ROOT/.git" ]; then
    echo "shesha repository not detected, performing first time setup..."
    cd $SHESHA_ROOT/../
    mkdir -p shesha && sudo chown -R docker:docker shesha
    sudo find shesha -type d -print0 | xargs -0 chmod 0777 && \
    sudo find shesha -type f -print0 | xargs -0 chmod 0666
    git clone https://github.com/ANR-COMPASS/shesha.git
fi

/start-jupyter.sh

bash
