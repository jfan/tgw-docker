#! /bin/bash

BASE_IMAGE=shesha-dev
BASE_DOCKERFILE=Dockerfile
GUI_IMAGE=shesha-dev-ssh
GUI_DOCKERFILE=Dockerfile.SSH

docker build -t $BASE_IMAGE -f $BASE_DOCKERFILE $PWD
docker build -t $GUI_IMAGE -f $GUI_DOCKERFILE $PWD
docker run -itd \
    --security-opt apparmor:unconfined \
    --cap-add=SYS_ADMIN \
    --gpus all \
    --env="DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --name $GUI_IMAGE \
    --volume "$PWD/shesha:/home/docker/shesha" \
    --rm \
    -p "18888:8888" \
    -p "12222:2222" \
    "$@" \
    $GUI_IMAGE

CONTAINER_HOSTNAME=`docker inspect --format='{{ .Config.Hostname }}' $(docker ps -q -f "name=$GUI_IMAGE")`

# Setting X permissions on host, make sure you do not kill the shell. Use 'exit' instead.
# Otherwise, xhost -local will not run.
xhost +local:$CONTAINER_HOSTNAME
docker attach $GUI_IMAGE
xhost -local:$CONTAINER_HOSTNAME
