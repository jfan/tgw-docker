#! /bin/bash

KRAKEN_ROOT=/home/compass/fish/kraken

if [[ -z "${SSH_ID_RSA}" ]]; then
    echo $SSH_ID_RSA > $HOME/.ssh/id_rsa
    chmod 600 $HOME/.ssh/id_rsa 
fi

if [ ! -f "$KRAKEN_ROOT/.git" ]; then
    echo "kraken repository not detected, performing first time setup..."
    cd $KRAKEN_ROOT/../
    mkdir -p "$KRAKEN_ROOT" && sudo chown -R compass:compass $KRAKEN_ROOT && ]
    sudo find $KRAKEN_ROOT -type d -print0 | xargs -0 chmod 0777 && \
    sudo find $KRAKEN_ROOT -type f -print0 | xargs -0 chmod 0666
    if [[ -z "${SSH_ID_RSA}" ]]; then
        git clone https://gitlab.obspm.fr/cosmic/ocean/kraken.git
    else
        git clone git@gitlab.obspm.fr:cosmic/ocean/kraken.git
    fi
fi

/start-jupyter.sh

nohup redis-server --protected-mode no &

bash
