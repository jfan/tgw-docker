# Useful variables
export FISH_ROOT=$HOME/fish
export CONDA_ROOT=$HOME/miniconda3
export PATH=$CONDA_ROOT/bin:$PATH
export PYTHONDONTWRITEBYTECODE=1

# OCTOPUS
export OCTOPUS_ROOT=$FISH_ROOT/octopus
export OCTOPUS_INSTALL_ROOT=$HOME/local/octopus
export PATH=$PATH:$OCTOPUS_INSTALL_ROOT/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$OCTOPUS_INSTALL_ROOT/lib
export PYTHONPATH=$PYTHONPATH:$OCTOPUS_INSTALL_ROOT/python
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$OCTOPUS_INSTALL_ROOT/lib/pkgconfig

#MARLIN
export MARLIN_ROOT=$FISH_ROOT/marlin
export MARLIN_INSTALL_ROOT=$HOME/local/marlin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MARLIN_ROOT/local/lib
export MILK_SHM_DIR=/milk/shm # Adapt to your setup, see OCTOPUS compilation part
export MILK_PROC_DIR=/milk/proc # Adapt to your setup, see OCTOPUS compilation part

#KRAKEN
export KRAKEN_ROOT=$FISH_ROOT/kraken
export PYTHONPATH=$PYTHONPATH:$KRAKEN_ROOT

#MAGMA definitions
export MAGMA_INSTALL_ROOT="$HOME/local/magma"
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MAGMA_INSTALL_ROOT/lib
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$MAGMA_INSTALL_ROOT/lib/pkgconfig
export GPU_TARGET="sm_70"

#COMPASS default definitions
export COMPASS_ROOT=$HOME/compass
export COMPASS_INSTALL_ROOT=$HOME/local/compass
export COMPASS_DO_HALF="ON" # set to ON if you want to use half precision RTC (needs SM>=60)
export NAGA_ROOT=$COMPASS_ROOT/naga
export SHESHA_ROOT=$COMPASS_ROOT/shesha
export LD_LIBRARY_PATH=$COMPASS_INSTALL_ROOT/lib:$LD_LIBRARY_PATH
export PYTHONPATH=$NAGA_ROOT:$SHESHA_ROOT:$COMPASS_INSTALL_ROOT/python:$PYTHONPATH
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:$COMPASS_INSTALL_ROOT/lib/pkgconfig
#third party lib path
export CUB_ROOT=$COMPASS_ROOT/tplib/cub

echo "Added environment variables from docker container"
