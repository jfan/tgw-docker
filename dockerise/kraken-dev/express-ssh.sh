#! /bin/bash

BASE_IMAGE=kraken-dev
BASE_DOCKERFILE=Dockerfile
GUI_IMAGE=kraken-dev-ssh
GUI_DOCKERFILE=Dockerfile.SSH

docker build -t $BASE_IMAGE -f $BASE_DOCKERFILE $PWD
docker build -t $GUI_IMAGE -f $GUI_DOCKERFILE $PWD
docker run -itd \
    --security-opt apparmor:unconfined \
    --cap-add=SYS_ADMIN --cap-add=SYS_NICE \
    --gpus all \
    --env="DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    --env "SSH_ID_RSA=$SSH_ID_RSA" \
    --env "GIT_NAME=$GIT_NAME" \
    --env "GIT_EMAIL=$GIT_EMAIL" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --volume="$PWD/kraken:/home/compass/fish/kraken" \
    --name $GUI_IMAGE \
    --rm \
    -p "28888:8888" \
    -p "22222:2222" \
    -p "26379:6379" \
    "$@" \
    $GUI_IMAGE

CONTAINER_HOSTNAME=`docker inspect --format='{{ .Config.Hostname }}' $(docker ps -q -f "name=$GUI_IMAGE")`

# Setting X permissions on host, make sure you do not kill the shell. Use 'exit' instead.
# Otherwise, xhost -local will not run.
xhost +local:$CONTAINER_HOSTNAME
docker attach $GUI_IMAGE
xhost -local:$CONTAINER_HOSTNAME
