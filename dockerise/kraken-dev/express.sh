#! /bin/bash

IMAGE_NAME=kraken-dev

docker build -t $IMAGE_NAME $PWD

docker run -it \
    --security-opt apparmor:unconfined \
    --cap-add=SYS_ADMIN --cap-add=SYS_NICE \
    --gpus all \
    --env="DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    --env "SSH_ID_RSA=$SSH_ID_RSA" \
    --env "GIT_NAME=$GIT_NAME" \
    --env "GIT_EMAIL=$GIT_EMAIL" \
    --volume="$PWD/kraken:/home/compass/fish/kraken" \
    --name $IMAGE_NAME \
    --rm \
    -p "28888:8888" \
    -p "26379:6379" \
    "$@" \
    $IMAGE_NAME
