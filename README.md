# tgw-docker

Passing on the development environment from the AITC Summer Research Scholarship 2019 (Kraken GUI Project).

This repository is intended to be used by the next researcher working on the Kraken Manager or 
Kraken RPC Server. It contains Dockerfiles made through multiple weeks of trial-and-error, taking 
build instructions from sessions with researchers at the Paris Observatory.

The Dockerfiles may end up needing to be tweaked for your use case, as they were specific to the 
project as-is.

Hopefully by the time the reader (you) have started the project, the build difficulties have been 
resolved. If so, continue on with the software as if this never existed. If not, it might be time 
to pick up some [Docker skills](https://www.tutorialspoint.com/docker/index.htm) if you haven't 
already and clone this repository.

For a less dramatic use case, these Dockerfiles could be used to containerise a development setup, 
particularly on the `colossus` server.
